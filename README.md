# `fibonacci-benchmark`

This is a personal experiment to benchmark Fibonacci sequence algorithms + WebAPI implementation in Rust and Golang programming languages.

## Problem

* Develop a simple web server
* Implement to solve Fibonacci sequence fast

## Assumption

Any rules other than `Problem` are not explicitly mentioned, so I made the following assumptions.
* I may use third party libraries to build web server
    * Because I believe this challenge is not intended to implement a web server from scratch (Using Socket + Parsing HTTP requests).
* No Caching 
    * Because I believe this challenge is intended to test skills of algorithm and web development, not caching. 
* Definition of `Fast`.
    * I assume Throughput + Latency
* Request / Response
    * Request
    ```
    http://localhost:8000/fib/<number>
    ```
    * Response
    ```json
    {"result":<value>}
    ```

## Implementation note

The first algorithm I wrote is using recursion.

```
F(n) = F(n-1) + F(n-2)
```

This code represents the mathematical formula of the Fibonacci sequence, and it is often seen as an introduction of Recursion in programing books.

I quickly implement it in Rust and run `hey` to check the performance. I realized the algorithm is too slow. The time complexity for this algorithm is exponential. Even optimized Rust program takes seconds to solve F(30).

Then, I started searching for better algorithms to solve Fibonacci number preferably in `O(N)` or `O(logn)`. It seems various algorithms are proposed.

#### Dynamic programming

This algorithms aims at reducing the repetitive computation from the recursion algorithm. It's as simple as the recursion algorithm and the time complexity is `O(n)`, but I didn't pick Dynamic programming because I found the more efficient algorithm `Matrix Exponentiation` which is discussed in the next section.

#### Matrix Exponentiation

This algorithms solves Fibonacci sequence by using Matrix Exponentiation. It's hard to describe the algorithm in detail here in Markdown. It boils down to this matrix problem. The time complexity is `O(logn)`.

```
| F(n)   |     | 1 1 | n-1  | F(1) |
|        |  =  |     |      |      |
| F(n-1) |     | 1 0 |      | F(0) |
```


## Rust

Rust version of the web server depends on `ActixWeb`. ActixWeb is one of the most popular web application framework for Rust, and is ranked at 5th in [TechEmpower Web Framework Benchmark](https://www.techempower.com/benchmarks/#section=data-r20&hw=ph&test=composite) as of August 21, 2021.


![](actix.png)

If Rust is not installed in your system, please run [this](https://rustup.rs). Now, run the web server

```
$ cd rust
$ cargo run --release
```

## Go

There seem to be [more choices of web application framework for Go](https://github.com/mingrammer/go-web-framework-stars). I picked [echo](https://github.com/labstack/echo) because the it has concise [documentation](https://github.com/smallnest/go-web-framework-benchmark) and good performance.

![](echo.png)

If Go is not installed in your system, please follow [this](https://golang.org/doc/install). Now, run the web server
```
$ cd go
$ go run .
```

## Rust vs. Go

### Environment

* MacBook Air 2020
    * OS: macOS BigSur
    * CPU: Apple M1
    * RAM: 16GB
* Use [hey](https://github.com/rakyll/hey) to measure http latency and throughput
* Run hey and the web server locally

### Rust

* Throughput: `131175.9839`
* Total Latency: `1.5247 secs`

```
hey -n 200000 -H "Content-Type: application/json" http://localhost:8000/fib/60 -h

Summary:
  Total:        1.5247 secs
  Slowest:      0.0375 secs
  Fastest:      0.0000 secs
  Average:      0.0004 secs
  Requests/sec: 131175.9839

  Total data:   4800000 bytes
  Size/request: 24 bytes

Response time histogram:
  0.000 [2]     |
  0.004 [198154]        |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  0.008 [514]   |
  0.011 [347]   |
  0.015 [127]   |
  0.019 [593]   |
  0.023 [254]   |
  0.026 [5]     |
  0.030 [1]     |
  0.034 [2]     |
  0.038 [1]     |
```

### Go

* Throughput: `116231.5776`
* Total Latency: `1.7207 secs`

```
$ hey -n 200000 http://localhost:8001/fib/60 -h

Summary:
  Total:        1.7207 secs
  Slowest:      0.0206 secs
  Fastest:      0.0000 secs
  Average:      0.0004 secs
  Requests/sec: 116231.5776

  Total data:   5000000 bytes
  Size/request: 25 bytes

Response time histogram:
  0.000 [1]     |
  0.002 [196970]        |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  0.004 [1874]  |
  0.006 [442]   |
  0.008 [287]   |
  0.010 [277]   |
  0.012 [126]   |
  0.014 [11]    |
  0.016 [9]     |
  0.019 [2]     |
  0.021 [1]     |
```

## Conclusion

Rust version of the web server appears to be faster than Go version, but Go version is a lot faster than I initially expected (Seeing [6](https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/rust-go.html), I expected Go is 2X slower). I can say Go has a good balance of `simplicity` + `performance`.

## References

1. Project Nayuki: Fast Fibonacci algorithms: https://www.nayuki.io/page/fast-fibonacci-algorithms
2. Solving the Fibonacci Sequence with Matrix Exponentiation: https://www.youtube.com/watch?v=EEb6JP3NXBI
3. がぶ飲みミルク珈琲: ベキ乗の高速化(log n): https://ttrsq.exblog.jp/24414256/
4. go-web-framework-stars: https://github.com/mingrammer/go-web-framework-stars
5. go-web-framework-benchmark: https://github.com/smallnest/go-web-framework-benchmark
6. The computer language benchmarks game: https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/rust-go.html