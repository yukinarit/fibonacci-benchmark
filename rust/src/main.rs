use actix_web::{get, web, App, HttpServer, Result};
use serde::Serialize;

#[derive(Debug, Serialize)]
struct Res {
    result: u64,
}

/// Solve fibonacci sequence with recursion.
fn fib_naive(n: u64) -> u64 {
    //println!("fib {}", n);
    if n < 2 {
        n
    } else {
        fib_naive(n - 1) + fib_naive(n - 2)
    }
}

type Matrix = [[u64; 2]; 2];

const F0: u64 = 0;

const F1: u64 = 1;

/// Solve fibonacci sequence with Matrix Exponentiation.
fn fib_matrix(n: u64) -> u64 {
    if n < 2 {
        return n;
    }

    let base = [[1, 1], [1, 0]];
    let matrix = power(base, n - 1);
    //println!("{:?}", matrix);
    matrix[0][0] * F1 + matrix[0][1] * F0
}

/// Compute "n" power of matrix.
fn power(mut a: Matrix, n: u64) -> Matrix {
    let mut res = [[1, 1], [1, 0]];
    if n <= 1 {
        a
    } else {
        let mut n = n - 1;
        while n > 0 {
            if n & 1 == 1 {
                res = multiply(res, a);
            }
            a = multiply(a, a);
            //println!("res={:?}, a={:?}", res, a);
            n >>= 1;
        }
        res
    }
}

/// Multiply 2 matrixes.
fn multiply(a: Matrix, b: Matrix) -> Matrix {
    [
        [
            a[0][0] * b[0][0] + a[0][1] * b[1][0],
            a[0][0] * b[1][0] + a[0][1] * b[1][1],
        ],
        [
            a[1][0] * b[0][0] + a[1][1] * b[1][0],
            a[1][0] * b[1][0] + a[1][1] * b[1][1],
        ],
    ]
}

#[get("/fib_naive/{n}")]
async fn route_fib_naive(n: web::Path<u64>) -> Result<web::Json<Res>> {
    Ok(web::Json(Res {
        result: fib_naive(*n),
    }))
}

#[get("/fib/{n}")]
async fn route_fib_matrix(n: web::Path<u64>) -> Result<web::Json<Res>> {
    if *n > 65 {
        return Err(actix_web::error::ErrorBadRequest(
            "n must not be greater than 65.",
        ));
    }

    Ok(web::Json(Res {
        result: fib_matrix(*n),
    }))
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(route_fib_naive)
            .service(route_fib_matrix)
    })
    .bind("0.0.0.0:8000")?
    .run()
    .await
}

#[test]
fn test_fib() {
    assert_eq!(0, fib_naive(0));
    assert_eq!(1, fib_naive(1));
    assert_eq!(1, fib_naive(2));
    assert_eq!(2, fib_naive(3));
    assert_eq!(3, fib_naive(4));
    assert_eq!(5, fib_naive(5));
    assert_eq!(8, fib_naive(6));
    assert_eq!(13, fib_naive(7));
    assert_eq!(21, fib_naive(8));
}

#[test]
fn test_fib_matrix() {
    assert_eq!(0, fib_matrix(0));
    assert_eq!(1, fib_matrix(1));
    assert_eq!(1, fib_matrix(2));
    assert_eq!(2, fib_matrix(3));
    assert_eq!(3, fib_matrix(4));
    assert_eq!(5, fib_matrix(5));
    assert_eq!(8, fib_matrix(6));
    assert_eq!(13, fib_matrix(7));
    assert_eq!(21, fib_matrix(8));
}
