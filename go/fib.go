package main

import (
	//"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Res struct {
	Result uint64 `json:"result"`
}

// Solve Fibonacci sequence with recursion.
func FibRecursion(n uint64) uint64 {
	if n < 2 {
		return n
	} else {
		return FibRecursion(n-1) + FibRecursion(n-2)
	}
}

type Matrix = [2][2]uint64

const (
	F0 = 0
	F1 = 1
)

// Solve fibonacci sequence with Matrix Exponentiation.
func FibMatrix(n uint64) uint64 {
	if n < 2 {
		return n
	}

	base := Matrix{{1, 1}, {1, 0}}
	matrix := power(base, n-1)
	return matrix[0][0]*F1 + matrix[0][1]*F0
}

// Compute "n" power of matrix.
func power(a Matrix, n uint64) Matrix {
	res := Matrix{{1, 1}, {1, 0}}
	if n <= 1 {
		return a
	} else {
		n -= 1
		for n > 0 {
			if n&1 == 1 {
				res = multiply(res, a)
			}
			a = multiply(a, a)
			//fmt.Println("res=", res, "a=", a, "n=", n)
			n >>= 1
		}
		return res
	}

}

// Multiply 2 matrixes.
func multiply(a Matrix, b Matrix) Matrix {
	return Matrix{
		{a[0][0]*b[0][0] + a[0][1]*b[1][0], a[0][0]*b[1][0] + a[0][1]*b[1][1]},
		{a[1][0]*b[0][0] + a[1][1]*b[1][0], a[1][0]*b[1][0] + a[1][1]*b[1][1]},
	}
}

func RouteFibRecursion(c echo.Context) error {
	n, err := strconv.ParseUint(c.Param("n"), 10, 64)
	if err != nil {
		return err
	}
	res := FibRecursion(n)
	return c.JSON(http.StatusOK, Res{res})
}

func RouteFibMatrix(c echo.Context) error {
	n, err := strconv.ParseUint(c.Param("n"), 10, 64)
	if err != nil {
		return err
	}
	res := FibMatrix(n)
	return c.JSON(http.StatusOK, Res{res})
}

func main() {
	e := echo.New()
	e.Use(middleware.Recover())

	e.GET("/fib_recursion/:n", RouteFibRecursion)
	e.GET("/fib/:n", RouteFibMatrix)
	e.Logger.Fatal(e.Start(":8001"))
}
