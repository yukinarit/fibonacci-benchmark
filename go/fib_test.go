package main

import (
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestFibRecursion(t *testing.T) {
	assert.Equal(t, uint64(0), FibRecursion(0))
	assert.Equal(t, uint64(1), FibRecursion(1))
	assert.Equal(t, uint64(1), FibRecursion(2))
	assert.Equal(t, uint64(2), FibRecursion(3))
	assert.Equal(t, uint64(3), FibRecursion(4))
	assert.Equal(t, uint64(5), FibRecursion(5))
	assert.Equal(t, uint64(8), FibRecursion(6))
	assert.Equal(t, uint64(13), FibRecursion(7))
	assert.Equal(t, uint64(21), FibRecursion(8))
}

func TestFibMatrix(t *testing.T) {
	assert.Equal(t, uint64(0), FibMatrix(0))
	assert.Equal(t, uint64(1), FibMatrix(1))
	assert.Equal(t, uint64(1), FibMatrix(2))
	assert.Equal(t, uint64(2), FibMatrix(3))
	assert.Equal(t, uint64(3), FibMatrix(4))
	assert.Equal(t, uint64(5), FibMatrix(5))
	assert.Equal(t, uint64(8), FibMatrix(6))
	assert.Equal(t, uint64(13), FibMatrix(7))
	assert.Equal(t, uint64(21), FibMatrix(8))
}

func TestRouteFibRecursion(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/fib/:n")
	c.SetParamNames("n")
	c.SetParamValues("8")

	RouteFibMatrix(c)
	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, "{\"result\":21}\n", rec.Body.String())
}

func TestRouteFibMatrix(t *testing.T) {
	e := echo.New()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/fib/:n")
	c.SetParamNames("n")
	c.SetParamValues("8")

	RouteFibMatrix(c)
	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, "{\"result\":21}\n", rec.Body.String())
}
